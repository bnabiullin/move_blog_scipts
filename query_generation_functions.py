def get_all_slugs_from_old_db(old):
    result_by_slug = {}
    result_by_category = {}
    cursor = old.cursor()
    query = """
        SELECT DISTINCT p.id, p.post_name, t.slug
        FROM wp_terms t
        INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id) 
        INNER JOIN wp_term_relationships tr ON (tt.term_taxonomy_id = tr.term_taxonomy_id)
        INNER JOIN wp_posts p ON (tr.object_id = p.ID)
        WHERE p.post_type = 'post' AND tt.taxonomy = 'category'"""
    cursor.execute(query)
    for data in cursor:
        result_by_slug[data[1]] = data[0]
        if data[2] not in result_by_category:
            result_by_category[data[2]] = []
        result_by_category[data[2]].append({'post_id': data[0], 'post_name': data[1]})
    cursor.close()
    return result_by_slug, result_by_category


def get_all_categories_from_new_db(new):
    result = {}
    cursor = new.cursor()
    query = """
        SELECT t.term_id, t.name, t.slug, tt.term_taxonomy_id
        FROM wp_terms t INNER JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id) 
        WHERE tt.taxonomy='category'"""
    cursor.execute(query)
    for data in cursor:
        result[data[1]] = {'term_id': data[0], 'slug': data[2], 'term_taxonomy_id': data[3]}
    cursor.close()
    return result


def create_insert_queries(lines, new_category, categories, posts_by_slug):
    category_info = categories[new_category]
    values_to_insert = [
        f'({posts_by_slug[slug]}, {category_info["term_taxonomy_id"]}, 0){"," if i < len(lines) - 1 else ";"} -- {slug},'
        for i, slug in enumerate(lines)]
    query_template_taxonomy = '\n'.join(values_to_insert)
    query_template_taxonomy = query_template_taxonomy[0:-1]
    queries_summed = f"""-- Запись в таблицу таксономий информации о категории "{new_category}"
INSERT INTO wp_term_relationships 
VALUES {query_template_taxonomy};

"""

    query_alter_count = f"""-- Изменение количества страниц в категории "{new_category}"
UPDATE wp_term_taxonomy
SET count = {len(values_to_insert)}
WHERE term_taxonomy_id = {category_info["term_taxonomy_id"]};


"""
    return queries_summed + query_alter_count
