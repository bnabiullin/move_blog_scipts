START TRANSACTION;

-- Удаляем в новой базе все метаданные для пользователей, которые не являются служебными пользователями
DELETE FROM wp_usermeta WHERE user_id NOT IN (1,3,4,12);

-- Удаляем wp_comments и wp_relevanssi_log неслужебных пользоваталей
DELETE FROM wp_comments WHERE user_id NOT IN (1,3,4,12);
DELETE FROM wp_relevanssi_log WHERE user_id NOT IN (1,3,4,12);

-- Удаляем всех неслужебных пользователей
DELETE FROM wp_users WHERE ID NOT IN (1,3,4,12);

-- Меняем пользователей для того, чтобы айдишники юзеров соответствовали айдишникам из старой базы 
-- -- ott должен быть 5
UPDATE wp_users users, wp_usermeta meta
SET users.ID = 5, meta.user_id = 5
WHERE users.ID = meta.user_id AND users.id = 1;

-- -- ottadmin должен быть 1
UPDATE wp_users users, wp_usermeta meta
SET users.ID = 1, meta.user_id = 1
WHERE users.ID = meta.user_id AND users.id = 3;

-- -- editor должен быть 2
UPDATE wp_users users, wp_usermeta meta
SET users.ID = 2, meta.user_id = 2
WHERE users.ID = meta.user_id AND users.id = 4;

-- -- Administrator должен быть 3
UPDATE wp_users users, wp_usermeta meta
SET users.ID = 3, meta.user_id = 3
WHERE users.ID = meta.user_id AND users.id = 12;

COMMIT;

-- -- Для служебных пользователей удаляем все неслужебные посты и их метаданные
/*
Удаляем посты и их таксономии
Больше о таксономиях можно прочитать тут:
https://wp-kama.ru/handbook/codex/taxonomies
*/
START TRANSACTION;

DELETE p, pm, tr
FROM wp_posts p
	INNER JOIN wp_postmeta pm ON (p.ID = pm.post_id)
    INNER JOIN wp_term_relationships tr ON (tr.object_id = p.ID)
WHERE p.post_type='post';

DELETE p, pm
FROM wp_posts p
	INNER JOIN wp_postmeta pm ON (p.ID = pm.post_id)
WHERE p.post_type='post';

-- Удаляем картинки, у которых коллизии с айдишниками старых постов
DELETE collision_posts
FROM db_test39.wp_posts collision_posts
INNER JOIN test.wp_posts old_posts
WHERE collision_posts.ID=old_posts.ID
    AND old_posts.post_type IN ('post')

-- На всякий случай удалим метаданные от постов, которых нет
DELETE m
FROM wp_postmeta m
INNER JOIN wp_posts p ON m.post_id=p.ID
WHERE p.ID IS NULL;

COMMIT;
