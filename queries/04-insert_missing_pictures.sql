-- Копируем метаданные картинок
INSERT INTO db_test39.wp_postmeta (post_id,meta_key,meta_value)
    SELECT i_meta.post_id, i_meta.meta_key, i_meta.meta_value
        FROM test.wp_posts images_old
            LEFT JOIN db_test39.wp_posts images_new ON (images_new.post_name = images_old.post_name)
            LEFT JOIN db_test39.wp_posts posts_new ON (images_old.post_parent = posts_new.ID)
            LEFT JOIN test.wp_postmeta i_meta ON (i_meta.post_id = images_old.ID)
        WHERE images_old.post_type='attachment'
            AND images_new.ID IS NULL
            AND posts_new.post_type='post';

-- Копируем картинки
INSERT INTO db_test39.wp_posts (
  post_author,
  post_date,
  post_date_gmt,
  post_content,
  post_title,
  post_excerpt,
  post_status,
  comment_status,
  ping_status,
  post_password,
  post_name,
  to_ping,
  pinged,
  post_modified,
  post_modified_gmt,
  post_content_filtered,
  post_parent,
  guid,
  menu_order,
  post_type,
  post_mime_type,
  comment_count
)
    SELECT
  images_old.post_author,
  images_old.post_date,
  images_old.post_date_gmt,
  images_old.post_content,
  images_old.post_title,
  images_old.post_excerpt,
  images_old.post_status,
  images_old.comment_status,
  images_old.ping_status,
  images_old.post_password,
  images_old.post_name,
  images_old.to_ping,
  images_old.pinged,
  images_old.post_modified,
  images_old.post_modified_gmt,
  images_old.post_content_filtered,
  images_old.post_parent,
  images_old.guid,
  images_old.menu_order,
  images_old.post_type,
  images_old.post_mime_type,
  images_old.comment_count
    FROM test.wp_posts images_old
        LEFT JOIN db_test39.wp_posts images_new ON (images_new.post_name = images_old.post_name)
        LEFT JOIN db_test39.wp_posts posts_new ON (images_old.post_parent = posts_new.ID)
    WHERE images_old.post_type='attachment'
        AND images_new.ID IS NULL
        AND posts_new.post_type='post';
