-- Копируем посты
INSERT db_test39.wp_posts
  SELECT * FROM test.wp_posts
    WHERE test.wp_posts.post_type = 'post'
    AND test.wp_posts.ID NOT IN (49420, 49474); -- auto drafts

-- Копируем метаданные постов
INSERT INTO db_test39.wp_postmeta (post_id,meta_key,meta_value)
  SELECT post_id,meta_key,meta_value
  FROM test.wp_postmeta LEFT JOIN test.wp_posts
    ON test.wp_postmeta.post_id = test.wp_posts.ID
  WHERE test.wp_posts.post_type = 'post'
    AND test.wp_posts.ID NOT IN (49420, 49474); -- auto drafts

-- Копируем пользователей
INSERT db_test39.wp_users
  SELECT * FROM test.wp_users
    WHERE test.wp_users.ID NOT IN (1,2,3,5); -- ottadmin, editor, Administrator, ott

-- Копируем метаданные пользователей
INSERT INTO db_test39.wp_usermeta (user_id,meta_key,meta_value)
  SELECT user_id,meta_key,meta_value
  FROM test.wp_usermeta LEFT JOIN test.wp_users
    ON test.wp_usermeta.user_id = test.wp_users.ID
  WHERE test.wp_users.ID NOT IN (1,2,3,5); -- ottadmin, editor, Administrator, ott