import json
from mysql import connector
from query_generation_functions import get_all_slugs_from_old_db, get_all_categories_from_new_db, create_insert_queries

host = 'localhost'
user = 'root'
password = 'admin'


def connect(schema):
    return connector.connect(user=user, password=password, host=host, database=schema)


new = connect('db_test39')
old = connect('wp_blog')

posts_by_slug, posts_by_category = get_all_slugs_from_old_db(old)
categories = get_all_categories_from_new_db(new)


def get_posts_by_category(prop_name: str, prop_value: list):
    slugs_for_categories = []
    errored_slugs = set()
    if prop_name == 'categories':
        for new_category in prop_value:
            posts = posts_by_category.get(new_category, [])
            if len(posts) == 0:
                print(f'Alarm! Empty category {new_category}')
            for post in posts:
                slug = post['post_name']

                if "russian-explorers" in slug and new_category == "weekend-plan":
                    print(f'Skipping "russian-explorers" for {new_category}: slug {slug}')
                elif slug in posts_by_slug:
                    slugs_for_categories.append(slug)
                else:
                    print(f'Alarm! Slug not in old db {slug}')
    return slugs_for_categories


def get_posts_by_links(prop_name: str, prop_value: list):
    slugs_for_links = []
    errored_slugs = set()
    if prop_name == 'links':
        for slug in prop_value:
            if slug in posts_by_slug:
                slugs_for_links.append(slug)
            else:
                print(f'Alarm! {slug}')
                errored_slugs.add(slug)
        for s in errored_slugs:
            prop_value.remove(s)
    return slugs_for_links


if __name__ == '__main__':
    """
    Как работает скрипт?
    1. Сначала мы должны собрать все посты из базы в словарь. Посты должны быть с категориями старой базы
    2. Потом собираем все категории, чтобы знать, к каким айдишниками привязываться
    3. Потом надо привязать каждый пост из мегаджейсона к категории
    3.1. Для каждого поста формируем запрос со вставкой в категорию, которая есть в джейсоне
    3.2. Подсчитать количество постов, которые не попадут ни в одну категорию
    4. ???
    5. PROFIT!
    
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!! Надо провалидировать постнеймы, есть ли они готовенькие для инсерта
    """

    f = open('links.json')
    links = json.load(f)

    with open('queries/03-insert-taxonomies.sql', 'w') as f:
        f.write('START TRANSACTION;\n\n')
        for category, subcategory in links.items():
            print(f'Обработка запросов категории {category}')
            for category_name, properties in subcategory.items():
                print(f'Формируем запросы для подкатегории {category_name}')
                lines = []
                for prop_name, prop_value in properties.items():
                    lines = [*lines, *get_posts_by_category(prop_name, prop_value)]
                    lines = [*lines, *get_posts_by_links(prop_name, prop_value)]
                if lines:
                    queries = create_insert_queries(lines, category_name, categories, posts_by_slug)
                    f.write(queries)
        f.write('\n\nCOMMIT;')
    print('')
