import json
from googleapiclient.discovery import build
from google.oauth2 import service_account
from time import sleep


SCOPES = ['https://spreadsheets.google.com/feeds']


class GoogleSheetsDAL:
    def __init__(self, spreadsheet_id, credentials, scopes):
        self.spreadsheet_id = spreadsheet_id
        self.credentials = credentials
        self.scopes = scopes
        self.sheet = None
        self.sleep_time = 1

    def initialize(self):
        credentials = service_account.Credentials.from_service_account_info(self.credentials, scopes=self.scopes)
        service = build('sheets', 'v4', credentials=credentials)
        self.sheet = service.spreadsheets()
        sleep(self.sleep_time)

    def create_row_range(self, sheet_name, left_column, right_column, from_row, rows_number):
        if not rows_number:
            return '{}!{}{}:{}'.format(sheet_name, left_column, from_row, right_column)
        return '{}!{}{}:{}{}'.format(sheet_name, left_column, from_row, right_column, from_row + rows_number - 1)

    def read_rows(self, sheet_id, sheet_name, left_column, right_column, from_row, rows_number):
        result = self.sheet.values() \
            .get(spreadsheetId=sheet_id,
                 range=self.create_row_range(sheet_name, left_column, right_column, from_row, rows_number)
                 ).execute()
        sleep(self.sleep_time)
        values = result.get('values', [])
        return values if len(values) else [[]]

    def read_first_row(self, sheet_id, sheet_name, left_column, right_column):
        values = self.read_rows(sheet_id, sheet_name, left_column, right_column, from_row=2, rows_number=1)
        return values[0]

    def read_everything(self, sheet_id, sheet_name):
        result = self.read_rows(sheet_id, sheet_name, left_column='A', right_column='Z', from_row=2, rows_number=None)
        return result

    def add_new_row(self, sheet_id):
        # Add new row
        requests = [
            {
                'insertDimension': {
                    "range": {
                        "sheetId": sheet_id,
                        "dimension": 'ROWS',
                        "startIndex": 1,
                        "endIndex": 2
                    },
                    "inheritFromBefore": False
                }
            }
        ]

        body = {
            'requests': requests
        }

        response = self.sheet.batchUpdate(spreadsheetId=self.spreadsheet_id, body=body).execute()
        sleep(self.sleep_time)

    def insert_row_data(self, review, sheet_name, left_column, right_column, inserted_row_number):
        values = [[*review]]

        data = [{'range': self.create_row_range(sheet_name, left_column, right_column,
                                                from_row=inserted_row_number, rows_number=1),
                 'values': values
                 }]

        body = {'valueInputOption': 'USER_ENTERED', 'data': data}

        self.sheet.values().batchUpdate(spreadsheetId=self.spreadsheet_id, body=body).execute()
        sleep(self.sleep_time)


class SpreadsheetDownloader:
    def __init__(self):
        self.credentials = {
            "type": "service_account",
            "project_id": "seop-186009",
            "private_key_id": "17ffe01f57b148df0478786d014a3500cbf9d7c1",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCollbFn/bfU2hs\n9JMUxI/qtMVyneocp0EaOHf7Tz5JNWXQrV3IbfcCmPqGjve+gFg8oC1GXdaZ/8rb\nruZUKz48GGxnyihhbbOAew4Zfrd15cAEIKEtZZSi+aX30vE/DHromi7hSPaGjhgv\n4B8/zm13s+NKpZMaDR1gnh2bloVMFD+8sWUzgneE22KrCaENezsNZIpsJ0YQUyR+\nZm6sMiB2gUcOklwxfRkm6aFCDRO0mLKcXx/58a+2TEKR3uzajneSgSFKMW4ncZ7G\n9MmLosiyMwGlUpR4LU/9L/vpDxZcoWSPn9nBjTXWoT0w1I0frh+nIpP+xt2nZAaf\noNNmx/eVAgMBAAECggEAJ8Nz+n95MWIhMIe8VErrfgq+woLEKBd8B8T3ab9eW9Kc\nLeziQKbNOt/dwVSh0x0/EqeeHil+d0rJrGizm3kqn+6LzSw1u4suKjj8EZ/dglGb\nKW4Pzh8WQJ19G7e1nq8hI1FQiuMUgJat1kb43+oe0DvN+bqVeHDM88+/gIXvcUq3\n07F7H6kGRxQ/FyYb86viGb9KdZ+tmMEK50e0N5jDU2184onqfridslqMZvLfrz/5\n2mV0ciYxO6U0lkBVBpVHfNL/9iJGET4BNdZ2ibgzCdph7bELpjfSwRSbVQKEQYdf\n6oxEKwpidiCRCn0R2WYw5oD4hO83p22a4P+WC44zqQKBgQDm+DZF4pYcAgnZ8zmM\n8DJpvMEo4zLqA7CJu4b5hT59UuU8foH70sj/EZ16KOKO8IIpgYyKkGgYmzQn4gPx\nVrG6tDBGi2SQgFl8Xc+AVXB7jEApXoOSSrKacmK+Pr3fpWGY2s3Sh/xn2v19dc0q\nHZhP64S8ZAg9Z87yh5CPNlzybQKBgQC623Rheu2xEaepA6TAq3+yetm9bTZNivVG\nmpo06x28v3mSc6GuKxrkCkWaRyGn+BNJsIxGfv6Trzo7sbt4Vrtcis5LCFyNXyai\ndPFTuZ9wpG637mNv+Xo2eBoqUM9qHOu7S57FirtHtUdTc7sSKjEZdu5K1uD7GeFl\nYh86j4AgyQKBgA6Y9WUpUu9mOZUqyCJeM/F22oZpBZjArvGlhnxSrYZiLNbL8A8o\nXUexp1BHMTpzfOZ+p5FCWzHi7ULwB+vC3Oy94qcyvCJd2kn/69Nikv9b99olfQ9L\n5UnZnOAFOx7dZvOwMH3z1kICFy9MuLDGug+jBPqbH+Ixo2S0qLBEcU9pAoGAfWRF\nXiELHrcVRsMuLSkpIVF1irueJD0qyX14HCjnJMFtSM0V43YDrqICATzYpgpAj0PN\nboVMOzjsoCBuCJxTUQ/IzetHiIsgtwY46BF5oDKIJh/wVLUawTCphloCRlgwDNj7\nK98kPEO3IJZaJB9yIX+A+3HlOjws2EcXY8nmlpkCgYA5jZDKa8XXVY6jGBxVQPgT\ndJEeGN2uHKNdTL8g0zNuGTjyydv2u6o+iGIOt94deng+votPS/cFNvgjbW3nqif/\nohlupOJxdxR89xDpphUCyEq3nDu/181dU1wRXXKtUeYgJKt+Tvhz9bTCLEsku8WM\nU6/11do1DLVJQapt/OEh5w==\n-----END PRIVATE KEY-----\n",
            "client_email": "seo-hotels-dev@seop-186009.iam.gserviceaccount.com",
            "client_id": "105981290248778569616",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/seo-hotels-dev%40seop-186009.iam.gserviceaccount.com"
        }
        self.spreadsheet_id = '1jMvncrjSjxnhys0ZFuFwgE-V6GgrNpPqnfwAR_Xtyv0'
        self.google_sheets_dal = GoogleSheetsDAL(self.spreadsheet_id, self.credentials, SCOPES)

        self.settings = {
            'migration': {
                'sheet_id_name': 'ЖД',
                'sheet_id_gid': 0,
                'columns_names': ('A', 'H')
            }
        }

    def run(self):
        self.google_sheets_dal.initialize()
        spreadsheet_contents = self.google_sheets_dal.read_everything(sheet_id=self.spreadsheet_id, sheet_name='Sheet1')
        rulebook = self.fill_rulebook(spreadsheet_contents)
        return rulebook

    def fill_rulebook(self, spreadsheet_contents):
        category, subcategory, rule = '', '', ''
        rulebook = {}
        for line in spreadsheet_contents[1:]:
            if len(line) < 3:
                print('Empty line')
                continue

            if line[0] != '':
                category = line[0]
            if line[1] != '':
                subcategory = line[1]
            if line[2] != '':
                rule = line[2]

            self.fill_rulebook_with_rule(rulebook, category, subcategory, rule)
        return rulebook

    @staticmethod
    def fill_rulebook_with_rule(rulebook, category, subcategory, rule):
        if category not in rulebook:
            rulebook[category] = {}

        if subcategory not in rulebook[category]:
            rulebook[category][subcategory] = {
                "links": [],
                "categories": []
            }

        if 'Все статьи' in rule:
            # appending in categories
            slug = rule.split('/')[-2]
            rulebook[category][subcategory]['categories'].append(slug)
        else:
            # appending links
            slug = rule.split('/')[-2]
            rulebook[category][subcategory]['links'].append(slug)

        return ''


downloader = SpreadsheetDownloader()
rulebook = downloader.run()
with open('links.json', 'w') as f:
    json.dump(rulebook, f, ensure_ascii=False, indent=2)
print('done')
